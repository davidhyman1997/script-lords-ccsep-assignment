Authors David, Graham, Hala

Build and run:

To build the dockerfile use "make build"

To run the dockerfile use "make run" or a better solution is to just use "python3 vuln_app.py"

Kill Dockerfile:

To kill the docker file use "docker ps" to find the image ID and then "docker kill image ID" to kill the process.

Exploit:

To exploit the app use a combination of %0d and %0a to inject CRLF into the /login page of the webapp. The vulnerable app will accept
these characters and thus the CRLF exploit is achieved.

The flask app is hosted on http://127.0.0.1:5000