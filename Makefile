build:
	docker build -t script_lords_app:latest .

run:
	docker run -d -p 5000:5000 script_lords_app:latest
