FROM ubuntu:18.04

RUN apt-get update && apt-get install -y python3 python3-pip

WORKDIR /assignment
COPY . /assignment

RUN pip3 --no-cache-dir install -r requirements.txt

EXPOSE 5001

ENTRYPOINT ["python3"]
CMD ["vuln_app.py"]
